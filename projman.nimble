# Package

version       = "1.1.1"
author        = "Maxb0tbeep"
description   = "A project manager CLI tool"
license       = "GPL-3.0-only"
srcDir        = "src"
binDir        = "build"
bin           = @["projman"]


# Dependencies

requires "nim >= 2.0.4"
requires "termstyle >= 0.1.0"
requires "yaml >= 2.1.1"
