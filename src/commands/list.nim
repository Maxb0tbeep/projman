import yaml, termstyle
import std/[os, streams, strformat, times, algorithm]
import ../utils/[conf, style]

type 
  ProjDir = object
    name: string
    path: string
  ProjectInfo = object
    path: string 
    time: Time
    parentDir: string

var
  projDirList: seq[ProjDir]
  projs:seq[ProjectInfo]
  sortedProjs:seq[ProjectInfo]
  latest:Time

proc compareProjInfo(a, b: ProjectInfo): int =
  if a.time > b.time:
    return -1  # a should come before b (descending order)
  elif a.time < b.time:
    return 1   # b should come before a
  else:
    return 0   # they are equal in time

proc List*(): void =
  Logo()

  let dirStream = newFileStream(fmt"{confDir}/directories.yaml")
  load(dirStream, projDirList)
  dirStream.close()

  for directory in projDirList:
    for x in walkDir(directory.path, true, true):
      if x.kind == pcDir and $x.path[0] != ".":
        let modTime:Time = getLastModificationTime(fmt"{directory.path}/{x.path}")
        projs.add(ProjectInfo(path: x.path, time: modTime, parentDir: directory.name))
     
  sort(projs, compareProjInfo)
  
  for proj in projs:
    echo "-> ".green, "[".magenta, proj.parentDir.blue, "] ".magenta, proj.path.cyan, " [".yellow, proj.time.format("yyyy-MM-dd").red, "]".yellow  
