import termstyle, yaml
import std/[os, strutils, streams, strformat]
import ../utils/conf

type ProjDir = object
  name: string
  path: string

var 
  projs:seq[string]
  projDirList:seq[ProjDir]
  halt:bool = false

proc Add*(): void =
  var pathArg = paramStr(2)

  if $pathArg[^1] == "/": # it will get confused if the last character is a /
    pathArg = pathArg[0 ..< pathArg.len - 1]

  if $pathArg[0] != "~" and $pathArg[0] != "/":
    echo "This path does not start with ".red, "~".yellow, " (home) or ".red, "/".yellow, " (root), which is required.".red
  else:
    try:
      # go thru every file in the given directory, returning the relative path
      # and throwing an error if it doesnt exist. also ignores hidden files.
      for x in walkDir(pathArg, true, true):
        if x.kind == pcDir and $x.path[0] != ".":
          projs.add(x.path)

      echo "This directory includes the following projects: ".green
      for proj in projs:
        echo "-> ".blue, proj.cyan

      stdout.write "Confirm adding to project directories [".white, "Y".green.bold, "/".white, "n".red.bold, "] ".white
      let confirmation = toLower(readLine(stdin))

      if confirmation == "y" or confirmation == "":
        # https://nimyaml.org/index.html
        let streamRead = newFileStream(fmt"{confDir}/directories.yaml", fmRead)
        try:
          load(streamRead, projDirList)
        except:
          echo "First directory."

        streamRead.close()

        let name = splitPath(pathArg).tail

        for i in projDirList:
          if i.name == name or i.path == pathArg:
            echo "A directory with this name or path already exists!".red
            halt = true
            
        if not halt:
          projDirList.add(ProjDir(name: $name, path: pathArg))

          let streamWrite = newFileStream(fmt"{confDir}/directories.yaml", fmWrite)
          Dumper().dump(projDirList, streamWrite)
          streamWrite.close()
    except IOError as e: 
      echo "This is not a valid path!".red
      echo e.msg
