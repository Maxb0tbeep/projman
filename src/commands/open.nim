import std/[os, strformat, osproc, streams]
import yaml, termstyle
import ../utils/[conf, scan]

type ProjDir = object
  name:string
  path:string

type Config = object
  editor:string

var 
  projDirList:seq[ProjDir]
  pathToOpen:string
  configuration:seq[Config]

proc Open*(name:string): void =
  let confStream = newFileStream(fmt"{confDir}/config.yaml")
  load(confStream, configuration)
  confStream.close()
  
  let pathToOpen:string = ScanProj(name)

  if pathToOpen == "1":
    echo "Could not find a project with the name ".red, name.yellow
  else:
    setCurrentDir(pathToOpen)
    let editorErr = execCmd(configuration[0].editor)
