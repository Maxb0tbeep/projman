import termstyle, yaml
import std/[os, strutils, streams, strformat]
import ../utils/[conf, scan]
import ./open.nim

type ProjDir = object
  name:string
  path:string

var 
  projDirList:seq[ProjDir]
  foundProjDir:bool = false

proc Create*(): void =
  try:
    var
      directory:string = paramStr(2) 
      projName:string = paramStr(3)

    if ScanProj(projName) != "1":
      raise newException(ValueError, "1")
    else:
      let dirStream = newFileStream(fmt"{confDir}/directories.yaml")
      load(dirStream, projDirList)
      dirStream.close()
      
      for projDir in projDirList:
        if projDir.name == directory:
          directory = projDir.path
          foundProjDir = true

      if foundProjDir != true:
        raise newException(ValueError, "1")

      let newProjPath = fmt"{directory}/{projName}"

      createDir(newProjPath)

      stdout.write fmt"Project created at ".green, newProjPath.cyan.bold, ", would you like to enter it? ".green, "[".white, "Y".green, "/".white, "n".red, "] ".white
      let confirmation = toLower(readLine(stdin))

      if confirmation == "y" or confirmation == "":
        Open(projName)
      
  except:
    echo "Invalid project directory or name".red
