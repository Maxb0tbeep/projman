# this file fucking sucks, but it looks pretty :)
import termstyle
import ../utils/style

proc Help*(): void =
  Logo()

  echo "\n",
    "$ ".yellow, "projman...\n\n",
    "-> ".bold.cyan, "add","                                    | ".bold.magenta, "add a directory to scan for projects\n",
    " ↳ ".bold.green, "$ ".bold.yellow, "projman ".blue, "add <directory>\n".cyan,
    " ↳ ".bold.red, "$ ".bold.yellow, "projman ".blue, "add ~/scripts/java\n\n".cyan,
    "-> ".bold.cyan, "remove","                                 | ".bold.magenta, "remove a project storage directory, will list\n",
    " ↳ ".bold.green, "$ ".bold.yellow, "projman ".blue, "remove\n\n".cyan,
    "-> ".bold.cyan, "list", " / ".red.bold, "ls","                              | ".bold.magenta, "list all scanned projects\n", 
    " ↳ ".bold.green, "$ ".bold.yellow, "projman ".blue, "ls\n\n".cyan,
    "-> ".bold.cyan, "create", "                                 | ".bold.magenta, "create a new project\n", 
    " ↳ ".bold.green, "$ ".bold.yellow, "projman ".blue, "create <directory> <project>\n".cyan,
    " ↳ ".bold.red, "$ ".bold.yellow, "projman ".blue, "create nim meow\n\n".cyan,
    "-> ".bold.cyan, "<project name>","                         | ".bold.magenta, "open a project\n",
    " ↳ ".bold.red, "$ ".bold.yellow, "projman ".blue, "meow\n\n".cyan,
    "-> ".bold.cyan, "help"," / ".red,"(no args)","                       | ".bold.magenta, "open THIS dialog!\n",
    " ↳ ".bold.green, "$ ".bold.yellow, "projman ".blue, "help\n\n".cyan,
    "!!! Please change your editor in ".red, "~/.config/projman/config.yaml".yellow, ", the default is vim.".red
