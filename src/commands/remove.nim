import yaml, termstyle
import std/[streams, strformat, strutils]
import ../utils/conf

type ProjDir = object
  name: string
  path: string

var 
  projDirList: seq[ProjDir]
  dirCount:int = 1

proc Remove*(): void =
  let dirStream = newFileStream(fmt"{confDir}/directories.yaml")
  load(dirStream, projDirList)
  dirStream.close()

  echo "Select which directory to remove (will not delete files). Enter nothing to cancel.".green
  
  for directory in projDirList: 
    echo fmt"(".yellow, dirCount.red,") ".yellow, directory.name.cyan, fmt" [{directory.path}]".white
    dirCount+=1

  stdout.write "-> ".blue
  let input = readLine(stdin)
  
  try:
    let 
      removeIndex:int = parseInt(input)-1
      testIfExists:ProjDir = projDirList[removeIndex] # check if the index is valid, will throw err if not
      writeStream = newFileStream(fmt"{confDir}/directories.yaml", fmWrite)

    projDirList.delete(removeIndex) 
    
    Dumper().dump(projDirList, writeStream)
    writeStream.close()
  except:
    echo "Cancelled.".green
