import std/[os, strformat]
import termstyle 

const
  user:string = getEnv("USER")
  confDir:string = fmt"/home/{user}/.config/projman"

proc VerifyConf*(): void =
  if not dirExists(confDir):
    echo "This is the first time running ".yellow,"projman".magenta.bold,", creating config files...".yellow

    createDir(confDir)

  if not fileExists(fmt"{confDir}/directories.yaml"):
    writeFile(fmt"{confDir}/directories.yaml", "")

  if not fileExists(fmt"{confDir}/config.yaml"):
    writeFile(fmt"{confDir}/config.yaml", """- editor: "vim" # set this to any editor command (eg. vim, nvim, lvim, nano, code, etc)""")

export confDir
