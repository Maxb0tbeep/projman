import std/[os, strformat, streams]
import yaml
import ./conf 

type ProjDir = object
  name:string
  path:string

type Config = object
  editor:string

var 
  projDirList:seq[ProjDir]
  pathToOpen:string
  configuration:seq[Config]

proc ScanProj*(name:string): string =
  let dirStream = newFileStream(fmt"{confDir}/directories.yaml")
  load(dirStream, projDirList)
  dirStream.close() 

  for directory in projDirList:
    for x in walkDir(directory.path, false, true):
        if x.kind == pcDir and $x.path[0] != "." and splitPath(x.path).tail == name:
          pathToOpen = x.path

  if pathToOpen != "":
    return pathToOpen
  else:
    return "1"
