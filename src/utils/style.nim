import termstyle
import std/[parsecfg, streams]

proc Logo*(): void = 
  const version = staticRead("../../projman.nimble").newStringStream.loadConfig.getSectionValue("", "version") # https://stackoverflow.com/a/77463816
  
  echo "",
     "                    _               \n".red,
     "   ___  _______    ".magenta,"(_)".red.bold,"_ _  ___ ____ \n".blue,
     "  / _ \\/ __/ _ \\  / /".magenta, "  ' \\/ _ `/ _ \\\n".blue,
     " / .__/_/  \\___/_/ /".magenta,"_/_/_/\\_,_/_//_/\n".blue,
     "/_/           |___/           ".magenta, "v".yellow, version.yellow, "\n"
