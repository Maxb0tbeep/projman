import std/os
import commands/[help, add, open, remove, list, create]
import utils/conf

when isMainModule:
  VerifyConf()

  if paramCount() == 0:
    Help()
  else:
    case paramStr(1):
      of "help":
        Help()
      of "add":
        Add()
      of "remove":
        Remove()
      of "list", "ls":
        List()
      of "create":
        Create()
      else:
        Open(paramStr(1))
