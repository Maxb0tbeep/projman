# projman 

![GitLab Last Commit](https://img.shields.io/gitlab/last-commit/Maxb0tbeep%2Fprojman?logo=git) ![Codacy grade](https://img.shields.io/codacy/grade/9395d1ad47cc47a1a85d51b301750de3?logo=codacy) ![AUR Version](https://img.shields.io/aur/version/projman?logo=arch%20linux) 

A project manager CLI tool for organization and convenience, written in Nim 👑

![demo](https://i.imgur.com/ZzfU2h8.gif)

## How it works

`projman` works by scanning directories you provide for subdirectories assumed to be projects. You are then able to easily list and open known projects, in a more convienent and efficient way than the typical `$ cd scripts/language/ProjectName` and `$ nvim`. With `projman`, this just becomes `$ projman ProjectName`.

## Installation

more options are being worked on !

### AUR

You can find the AUR package at [https://aur.archlinux.org/packages/projman](https://aur.archlinux.org/packages/projman), or use an AUR helper

`$ yay -S projman`

`$ paru -S projman`

### Build from source

1. `# pacman -S nim git`
2. clone this repository with git
3. `$ nimble build`
4. the compiled binary will be in `build/` :)

## Command guide

### `add`

provide a path starting with `~` or `/` and it will remember this as a parent folder for your projects to be stored in 

`$ projman add ~/scripts/java`

### `remove`

will list all parent directories and give you an option for which to remove

`$ projman remove`

### `list`

lists all projects located in provided parent directories

`$ projman list`

### `<project name>`

will try to open a project, scanning parent folders for a name match

`$ projman Crescendo2024`

### `help`

basically everything listed here but in the package rather than markdown :)

`$ projman help`

---

![projman](media/projman.png)
